import React, { Component } from 'react';
import './BuildControls.css'
import BuildControl from './BuildControl/BuildControl'

const controls = [
    {label: 'Salad', type: 'salad'},
    {label: 'Cheese', type: 'cheese'},
    {label: 'Bacon', type: 'bacon'},
    {label: 'Meat', type: 'meat'},
]

const BuildControls = (props) => {
    const controlPanel = controls.map(control => {
        return <BuildControl
            label={control.label}
            type={control.type}
            key={control.type}
            added={()=>props.addIngredient(control.type)}
            removed={()=>props.removeIngredient(control.type)}
            disabled={props.disabled[control.type]}
        />
    });

    return (<div className="BuildControls">
        {controlPanel}
        <button className="OrderButton" onClick={props.ordered}>Order Now!</button>
    </div>);
}

export default BuildControls
