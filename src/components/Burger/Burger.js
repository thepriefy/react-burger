import React, { Component } from 'react';
import './Burger.css'
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'

const Burger = (props) => {

    var outputIngredient = Object.keys(props.ingredients)
        .map(ingredient => {
            const count_ingredient = props.ingredients[ingredient];

            var arrayIngredient = [];

            if(count_ingredient > 0){
                for(var i=0; i<count_ingredient; i++)
                {
                    arrayIngredient.push(<BurgerIngredient type={ingredient} key={ingredient + i}/>);
                }
            }

            return arrayIngredient;
        });

    return (
        <div className="Burger">
            <BurgerIngredient type="bread-top"/>
            {outputIngredient}
            <BurgerIngredient type="bread-bottom"/>
        </div>
    );
}

export default Burger;