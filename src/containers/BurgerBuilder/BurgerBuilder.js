import React, { Component } from 'react';
import Burger from '../../components/Burger/Burger'
import BuildControl from '../../components/Burger/BuildControls/BuildControls'

class BurgerBuilder extends Component {
    INGREDIENTS_PRICES = {
        salad: 10,
        cheese: 20,
        bacon: 30,
        meat: 40
    };

    state = {
        ingredients: {
            salad: 0,
            cheese: 0,
            bacon: 0,
            meat: 0
        },
        total_price: 0
    };

    addIngredientHandler = (type) => {
        // alert("MORE : " + type)
        const newIngredient = {...this.state.ingredients};
        newIngredient[type] += 1;

        const updatedPrice = this.state.total_price + this.INGREDIENTS_PRICES[type];

        this.setState({
            ingredients: newIngredient,
            total_price: updatedPrice
        });
    }

    removeIngredientHandler = (type) => {
        // alert("LESS : " + type)
        const newIngredient = {...this.state.ingredients};
        newIngredient[type] -= 1;

        const updatedPrice = this.state.total_price - this.INGREDIENTS_PRICES[type];

        this.setState({
            ingredients: newIngredient,
            total_price: updatedPrice
        });
    }

    orderBurger = () => {
        const ingredients = {...this.state.ingredients};

        let message = Object.keys(ingredients)
            .map(ingredient => {
                return ingredient + " : " + ingredients[ingredient];
            })

        message += " || total price = " + this.state.total_price;

        alert(message);
    }

    render() {
        const disableControls = {...this.state.ingredients};

        for(let ingredient in disableControls)
        {
            disableControls[ingredient] = disableControls[ingredient] <= 0;
        }

        return (
            <div>
                <Burger ingredients={this.state.ingredients}/>
                <BuildControl
                    addIngredient={this.addIngredientHandler}
                    removeIngredient={this.removeIngredientHandler}
                    disabled={disableControls}
                    ordered={this.orderBurger}
                />
            </div>
        );
    }
}

export default BurgerBuilder;